//Product
#include <stdio.h>
int main() {
    float n1, n2, product;
    printf("Enter First number: ");
    scanf("%f", &n1);
    printf("Enter Second number: ");
    scanf("%f", &n2);
    product = n1 * n2;
    printf("Product = %.2f", product);
    return 0;
}
